import "module-alias/register";
import express, { json } from "express";
import cors from "cors";
import path from "path";
const routes = require("./app/routes");
import dotenv from "dotenv";

dotenv.config();

const PORT = process.env.PORT || 5503;

const app = express();
app.use(json({ limit: "100mb" }));

app.use(cors());

const corsOptions = cors({ origin: true });

app.options("*", corsOptions);

app.use("/api", routes);

app.use(
  "/media",
  express.static(
    path.join(
      __dirname,
      process.env.MODE === "development" ? "./dist/media" : "./media"
    )
  )
);

app.get("/", (request, response) => {
  return response.send(
    `<div style="font-family: Source Sans Pro, Lucida Grande, sans-serif;display:flex;flex-direction:column;justify-content:center;align-items:center;color:#333;">
      <h1>Pizza Boilerplate API</h1>
      <br>
      <p>to access API routes use <strong>/api</strong> </p>
      <hr />
      <p>Server running on port: <strong>${PORT}</strong></p>
      <p>Environment mode: <strong>${process.env.MODE}</strong></p>
      <p>v${require(`${process.env.MODE === "production" ? '..' : '.'}/package.json`).version}</p>
     <div>`
  );
});

app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
  console.log(`Environment mode: ${process.env.MODE}`);
});
