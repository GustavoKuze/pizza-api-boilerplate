interface IUserSearchResult {
  id: number;
  name: string;
  email: string;
  avatar: string;
  username: string;
}

export default IUserSearchResult;
