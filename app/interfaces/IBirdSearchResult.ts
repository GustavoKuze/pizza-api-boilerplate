interface IBirdSearchResult {
  id: number;
  name: string;
  scientificName: string;
  pictureUrl: string;
}

export default IBirdSearchResult;
