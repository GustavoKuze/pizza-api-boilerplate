interface IUserComment {
  id: number;
  userId: number;
  pictureId: number | undefined;
  commentId: number | undefined;
  text: string;
  date?: Date;
}

export default IUserComment;
