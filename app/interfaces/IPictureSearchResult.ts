interface IPictureSearchResult {
  id: number;
  name: string;
  description: string;
  url: string;
  location: string;
  userId: string;
  birdId: string;
  date: Date;
}

export default IPictureSearchResult;
