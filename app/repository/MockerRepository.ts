import User from "@models/User";
import chance from "@services/chance";
import BaseRepository from "./BaseRepository";
import UserRepository from "./UserRepository";
// @ts-ignore
import moment from "moment";
import { hashPassword } from "@utils/hasher";
import Picture from "@models/Picture";
import FeedRepository from "./FeedRepository";
import UserComment from "@models/UserComment";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

interface MockerRepositoryModel {
  generateFakeUsers(quantity: number): Promise<number[] | null>;
}

class MockerRepository extends BaseRepository implements MockerRepositoryModel {
  async generateFakeUsers(quantity: number): Promise<number[] | null> {
    let generatedIDList: number[] = [];

    const commonPassword = await hashPassword("Hot123123");

    for (let i = 0; i < quantity; i++) {
      try {
        const user = new User(
          0,
          chance.name({ nationality: "en" }),
          chance.email(),
          chance.phone({ country: "br" }),
          `${chance.name({ nationality: "en" }).replace(/[ ]/gm, '')}#${chance.integer({ min: 1, max: 99999 })}`,
          undefined,
          undefined,
          false,
          commonPassword
        );

        const userRepo = new UserRepository();
        const newUserIdRaw = await userRepo.signUp(user);
        const newUserId = parseInt(`${newUserIdRaw}`, 10);

        if (newUserId) {
          generatedIDList = [...generatedIDList, newUserId];
        }
      } catch (error) {
        if (!error.message.includes('email_unique'))
          return Promise.reject(new Error(error.message));
      }
    }

    return generatedIDList;
  }

  async generateFakePictures(
    quantity: number,
    withComments = true
  ): Promise<number[] | null> {
    let generatedIDList: number[] = [];
    const birdsResult = await prisma.bird.findMany();

    for (let i = 0; i < quantity; i++) {
      try {
        const pickedBird = chance.pickone(birdsResult);
        const pickedLocation = chance.pickone(['Porto Alegre, RS', 'Gravataí, RS', 'Tubarão, SC', 'São Carlos, SP', 'São Paulo, SP']);

        const picture = new Picture(
          0,
          chance.paragraph(),
          pickedBird.picture_url,
          pickedLocation,
          chance.integer({ min: 175, max: 205 }),
          pickedBird.id,
          new Date(),
        );

        const feedRepo = new FeedRepository();
        const newPictureIdRaw = await feedRepo.createNewPicture(picture);

        const newPictureId = parseInt(`${newPictureIdRaw.id}`, 10);

        if (withComments) {
          for (let j = 0; j < quantity; j++) {
            const comment = new UserComment(
              0,
              chance.integer({ min: 10, max: 500 }),
              newPictureId,
              undefined,
              chance.paragraph({ sentences: 1 }),
              new Date()
            );

            const newCommentIdRaw = await feedRepo.createNewComment(comment);
            const newCommentId = parseInt(`${newCommentIdRaw.id}`, 10);
            if (!newCommentId)
              throw new Error("Não foi possível criar o comentário");
          }
        }

        if (newPictureId) {
          generatedIDList = [...generatedIDList, newPictureId];
        }
      } catch (error) {
        return Promise.reject(new Error(error.message));
      }
    }

    return generatedIDList;
  }
}

export default MockerRepository;
