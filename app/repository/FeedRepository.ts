import { FEED_PAGE_SIZE } from "@constants/pagination";
import Bird from "@models/Bird";
import Like from "@models/Like";
import Picture from "@models/Picture";
import UserComment from "@models/UserComment";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()
import { getFriendlyDate } from "@utils/index";
import BaseRepository from "./BaseRepository";

interface FeedRepositoryModel {
  createNewPicture(picture: Picture): Promise<any>;
  createNewComment({
    text,
    pictureId,
    commentId,
    userId,
  }: UserComment): Promise<any>;
  getAllComments(
    pictureId: number,
    limit?: number,
    showAll?: Boolean
  ): Promise<any[] | null>;
  getPictureLikesCount(pictureId: number): Promise<number | null>;
  getCommentLikesCount(commentId: number): Promise<number | null>;
  getUserPictures(userId: number, page: number): Promise<any[] | null>;
  like(like: Like): Promise<Boolean>;
  removeLike(like: Like): Promise<Boolean>;
  didUserLikedAlready(
    userId: number,
    pictureId?: number,
    commentId?: number
  ): Promise<Boolean>;
  getLikesDetails(like: Like): Promise<any | null>;
  getBirdDetails(birdId: number): Promise<Bird | null>;
}

class FeedRepository extends BaseRepository implements FeedRepositoryModel {
  async createNewPicture({
    description,
    url,
    location,
    userId,
    birdId,
  }: Picture): Promise<any> {

    const result = await prisma.picture.create({
      data: {
        location,
        url,
        description,
        id_bird: birdId,
        id_user: userId,
      }
    });

    return result;
  }

  async createNewComment({
    text,
    pictureId,
    commentId,
    userId,
  }: UserComment): Promise<any> {

    const result = await prisma.comment.create({
      data: {
        text,
        id_picture: pictureId,
        id_user: userId,
        id_comment: commentId,
      }
    });

    return result;
  }

  async getCommentReplies(commentId: number): Promise<any[]> {
    let result = await prisma.$queryRaw(`SELECT cr.id,
    cr.text,
    u.avatar,
    u.id as userId,
    u.name as userName
    FROM comment cp RIGHT JOIN comment cr 
    ON cp.id = cr.id_comment INNER JOIN user u ON cr.id_user = u.id
    WHERE cp.id = ?;`, commentId);

    if (result && result.length > 0) {
      result = await Promise.all(
        result.map(async (comment) => {
          const thisCommentReplies = await this.getCommentReplies(comment.id);
          return {
            ...comment,
            replies:
              thisCommentReplies && thisCommentReplies.length > 0
                ? thisCommentReplies
                : [],
            likes: await this.getCommentLikesCount(comment.id),
          };
        })
      );
    }

    return result;
  }

  async getAllComments(
    pictureId: number,
    limit = 0,
    showAll = false
  ): Promise<any[] | null> {
    let result = await prisma.$queryRaw(`SELECT c.id,
    c.text,
    c.date,
    u.avatar,
    u.id as userId,
    u.name as userName
    FROM picture p RIGHT JOIN comment c 
    ON c.id_picture = p.id INNER JOIN user u ON c.id_user = u.id
    WHERE p.id = ?;`, pictureId);

    if (result && result.length > 0) {
      result = await Promise.all(
        result.map(async (comment) => ({
          ...comment,
          replies: await this.getCommentReplies(comment.id),
          likes: await this.getCommentLikesCount(comment.id),
          date: getFriendlyDate(comment.date),
        }))
      );

      result =
        result.length > limit && !showAll
          ? !!limit
            ? result.slice(0, limit)
            : result
          : result;
    }

    return result;
  }

  async didUserLikedAlready(
    userId: number,
    pictureId?: number,
    commentId?: number
  ): Promise<Boolean> {
    const result = await prisma.$queryRaw(`SELECT l.id FROM ${pictureId ? "picture" : "comment"
      } p RIGHT JOIN 
  \`like\` l ON p.id = l.id_${pictureId ? "picture" : "comment"
      } RIGHT JOIN user u ON u.id = l.id_user
  WHERE p.id = ? AND u.id = ?;`, pictureId || commentId, userId);

    return result.length > 0;
  }

  async like({ userId, commentId, pictureId }: Like): Promise<Boolean> {
    if (await this.didUserLikedAlready(userId, pictureId, commentId))
      return false;

    const queryCreateLike = !!pictureId
      ? `INSERT INTO \`like\` (id_user, id_picture) VALUES (?, ?);`
      : `INSERT INTO \`like\` (id_user, id_comment) VALUES (?, ?);`;

    const result = await prisma.$executeRaw(queryCreateLike,
      userId,
      pictureId || commentId,
    );

    return !!result;
  }

  async removeLike({ userId, commentId, pictureId }: Like): Promise<Boolean> {
    if (!(await this.didUserLikedAlready(userId, pictureId, commentId)))
      return false;

    const queryCreateLike = `DELETE FROM \`like\` WHERE id_user = ? AND id_${!!pictureId ? "picture" : "comment"
      } = ?;`;

    const result = await prisma.$executeRaw(queryCreateLike,
      userId,
      pictureId || commentId,
    );

    return !!result;
  }

  async getUserPictures(userId: number, page = 1): Promise<any[] | null> {
    const user = await prisma.user.findFirst({ where: { id: userId } });

    let result = await prisma.picture.findMany({
      where: { id_user: userId },
      include: { bird: true, },
      ...this.paginate(page),
    });

    if (result.length > 0) {
      // @ts-ignore
      result = await Promise.all(
        result.map(async ({ id_user, id_bird, bird, ...picture }) => {
          return ({
            userId: id_user,
            birdId: id_bird,
            birdName: bird?.name,
            ...picture,
            comments: await this.getAllComments(picture.id, 2),
            likes: await this.getPictureLikesCount(picture.id),
            // @ts-ignore
            date: getFriendlyDate(picture.date),
            userAvatar: user?.avatar,
            userFullName: user?.name,
          })
        })
      );
    }

    return result;
  }

  async getPictureLikesCount(pictureId: number): Promise<number | null> {
    const queryPictureLikesCount = `SELECT count(l.id) likes
    FROM picture p RIGHT JOIN \`like\` l ON p.id = l.id_picture WHERE p.id = ?;`;

    let result = await prisma.$queryRaw(queryPictureLikesCount, pictureId);

    return result[0].likes;
  }

  async getCommentLikesCount(commentId: number): Promise<number | null> {
    const queryPictureLikesCount = `SELECT count(l.id) likes
    FROM comment c RIGHT JOIN \`like\` l ON c.id = l.id_comment WHERE c.id = ?;`;

    let result = await prisma.$queryRaw(queryPictureLikesCount, commentId);

    return result[0].likes;
  }

  async getLikesDetails(like: Like): Promise<any | null> {
    const queryLikes = `select u.id userId, u.name userName, u.avatar avatar, u.email email, l.date, l.id likeId
    from ${like.pictureId ? "picture" : "comment"} p right join \`like\` l
    on l.id_${like.pictureId ? "picture" : "comment"} = p.id left join user u
    on l.id_user = u.id
    where p.id = ?;`;

    let result = await prisma.$queryRaw(queryLikes, like.pictureId || like.commentId);
    return result;
  }

  async getBirdDetails(birdId: number): Promise<Bird | null> {
    const {
      class: birdClass,
      description,
      family,
      kingdom,
      model_label,
      order,
      phylum,
      scientific_name,
      sound_url,
      thumbnail_url,
    } = (await prisma.bird.findFirst({
      where: { id: birdId }
    })) || {};

    if (!model_label) throw new Error('Espécie não encontrada.')

    return new Bird(
      birdId,
      scientific_name || '',
      phylum || '',
      order || '',
      kingdom || '',
      birdClass || '',
      family || '',
      thumbnail_url || '',
      sound_url || '',
      description || '',
      model_label || '',
    );
  }
}

export default FeedRepository;




