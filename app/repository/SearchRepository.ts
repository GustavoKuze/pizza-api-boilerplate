import BirdSearchResult from "@models/BirdSearchResult";
import PictureSearchResult from "@models/PictureSearchResult";
import UserSearchResult from "@models/UserSearchResult";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()
interface SearchRepositoryModel {
  searchBirds(name: string): Promise<BirdSearchResult[] | null>;
  searchPictures(name: string): Promise<PictureSearchResult[] | null>;
  searchUsers(name: string): Promise<UserSearchResult[] | null>;
}

class SearchRepository implements SearchRepositoryModel {
  async searchBirds(name: string): Promise<BirdSearchResult[] | null> {
    try {
      const searchResult = await prisma.bird.findMany({
        where: {
          OR: [{
            name: { startsWith: name }
          }]
        }
      });

      const mappedResults = searchResult.map(
        (result: any) =>
          this._mapToBirdSearchResult(result)
      );

      return mappedResults.length > 10
        ? mappedResults.slice(0, 10)
        : mappedResults;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  async searchPictures(name: string): Promise<PictureSearchResult[] | null> {
    try {
      const searchResult = await prisma.bird.findMany({
        where: { name: { startsWith: name }, picture: { some: { id: { gt: 0 } } } },
        include: { picture: true }
      });

      let mappedResults: any = [];

      if (searchResult.length > 0) {
        searchResult.forEach(bird => {
          mappedResults = [...mappedResults, ...bird.picture.map(
            (picture: any) =>
              new PictureSearchResult(
                picture.id,
                bird.name,
                picture.description,
                picture.url,
                picture.location,
                picture.id_user,
                picture.id_bird,
                picture.date
              )
          )]
        })
      }

      return mappedResults.length > 35
        ? mappedResults.slice(0, 35)
        : mappedResults;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  async searchUsers(name: string): Promise<UserSearchResult[] | null> {
    try {
      const searchResult = await prisma.user.findMany({
        where: {
          OR: [{
            name: { startsWith: name }
          }]
        }
      });

      const mappedResults = searchResult.map(
        (result: any) =>
          new UserSearchResult(
            result.id,
            result.name,
            result.email,
            result.avatar,
            result.username,
          )
      );

      return mappedResults.length > 35
        ? mappedResults.slice(0, 35)
        : mappedResults;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  _mapToBirdSearchResult(bird: any): BirdSearchResult {
    return new BirdSearchResult(
      bird.id,
      bird.name,
      bird.scientific_name,
      bird.picture_url,
      bird.thumbnail_url,
    )
  }
}

export default SearchRepository;
