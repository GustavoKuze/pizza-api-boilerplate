import User from "@models/User";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

interface UserRepositoryModel {
  signUp(user: User): Promise<number | string | null>;
  getAll(): Promise<Array<User>>;
}

class UserRepository implements UserRepositoryModel {
  async signUp(user: User): Promise<any | null> {
    try {

      const userParam: any = user;

      const result = await prisma.user.create({
        data: userParam
      })

      return result;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  async getByEmail(email: string): Promise<User | null> {
    try {
      const user = await prisma.user.findUnique({
        where: { email },
      });

      return !!user ? this._mapToUser(user, true) : null;
    } catch (error) {
      return Promise.reject(new Error(error));
    }
  }

  async isEmailRegistered(email: string) {
    try {
      const user = await this.getByEmail(email);

      return !!user;
    } catch (error) {
      return Promise.reject(new Error(error));
    }
  }

  async getById(id: string): Promise<User | null> {
    try {
      const user = await prisma.user.findUnique({
        where: { id: parseInt(id, 10) },
      });

      return !!user ? this._mapToUser(user) : null;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  async getAll(): Promise<Array<User>> {
    try {
      const dbUsers = await prisma.user.findMany({
        where: { disabled: false }
      });

      const users = dbUsers.map((user) => this._mapToUser(user));

      return users;
    } catch (error) {
      return Promise.reject(new Error(error));
    }
  }

  _mapToUser(user: any, withPassword: Boolean = false): User {
    return new User(
      user?.id || 0,
      user?.name || '',
      user?.email || '',
      user?.phone || '',
      user?.username || '',
      user?.created_at || undefined,
      user?.updated_at || undefined,
      user?.disabled || false,
      withPassword ? user?.password || '' : undefined,
      user?.avatar || '',
    )
  }
}

export default UserRepository;
