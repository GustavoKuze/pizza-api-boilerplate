import Order from '@models/Order';
import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();
import BaseRepository from './BaseRepository';

interface OrderModel {
  createOrder(order: Order): Promise<any | null>;
  listOrders(userId: number, page: number): Promise<any[] | null>;
  getOrder(id: number): Promise<any | null>;
}

class OrderRepository extends BaseRepository implements OrderModel {
  async createOrder({
    id,
  }: Order): Promise<any> {
    const result = await prisma.order.create({
      data: {
        id
      }
    });

    return result;
  }

  async listOrders(userId: number, page: number): Promise<any[]> {
    try {
      const result = await prisma.order.findMany({
        where: { id_user: userId },
        ...this.paginate(page),
      });

      return result;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }

  async getOrder(id: number): Promise<any> {
    try {
      const result = await prisma.order.findFirst({
        where: { id }
      });

      return result;
    } catch (err) {
      return Promise.reject(new Error(err));
    }
  }
}

export default OrderRepository;
