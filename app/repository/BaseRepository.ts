/* eslint-disable max-len */
/* eslint-disable class-methods-use-this */

import { FEED_PAGE_SIZE } from "@constants/pagination";

class BaseRepository {
    paginate(page: number) {
        return {
            skip: FEED_PAGE_SIZE * (page - 1),
            take: FEED_PAGE_SIZE,
        }
    }
}

export default BaseRepository;
