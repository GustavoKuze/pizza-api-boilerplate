class Like {
  constructor(
    public id: number,
    public userId: number,
    public pictureId?: number | undefined,
    public commentId?: number | undefined
  ) {}
}

export default Like;
