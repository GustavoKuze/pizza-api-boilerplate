import IUserSearchResult from "@interfaces/IUserSearchResult";

class UserSearchResult implements IUserSearchResult {
  constructor(
    public id: number,
    public name: string,
    public email: string,
    public avatar: string,
    public username: string
  ) {}
}

export default UserSearchResult;
