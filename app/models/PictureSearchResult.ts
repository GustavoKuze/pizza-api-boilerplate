import IPictureSearchResult from "@interfaces/IPictureSearchResult";

class PictureSearchResult implements IPictureSearchResult {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public url: string,
    public location: string,
    public userId: string,
    public birdId: string,
    public date: Date
  ) {}
}

export default PictureSearchResult;
