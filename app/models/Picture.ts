class Picture {
  constructor(
    public id: number,
    public description: string,
    public url: string,
    public location: string,
    public userId: number,
    public birdId: number,
    public date?: Date
  ) {}
}

export default Picture;
