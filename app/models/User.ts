class User {
  constructor(
    public id: number,
    public name: string,
    public email: string,
    public phone: string,
    public username: string,
    public createdAt?: Date,
    public updatedAt?: Date,
    public disabled?: Boolean,
    public password?: string,
    public avatar?: string,
  ) {}
}

export default User;
