import IBirdSearchResult from "@interfaces/IBirdSearchResult";

class BirdSearchResult implements IBirdSearchResult {
  constructor(
    public id: number,
    public name: string,
    public scientificName: string,
    public pictureUrl: string,
    public thumbnailUrl: string,
  ) {}
}

export default BirdSearchResult;
