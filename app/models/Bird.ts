class Bird {
  constructor(
    public id: number,
    public scientificName: string,
    public phylum: string,
    public order: string,
    public kingdom: string,
    public birdClass: string,
    public family: string,
    public pictureUrl: string,
    public audioUrl: string,
    public description: string,
    public modelLabel: string
  ) {}
}

export default Bird;
