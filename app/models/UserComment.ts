import IUserComment from "@interfaces/IUserComment";

class UserComment implements IUserComment {
  constructor(
    public id: number,
    public userId: number,
    public pictureId: number | undefined,
    public commentId: number | undefined,
    public text: string,
    public date?: Date
  ) {}
}

export default UserComment;
