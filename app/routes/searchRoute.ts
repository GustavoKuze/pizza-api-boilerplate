import { Router } from "express";
import { 
    searchBirdNames,
    searchUsers,
    searchPictures
} from "@controllers/searchController";

const router = Router();

const BASE_ROUTE = "/search";

router
.get(`${BASE_ROUTE}/birds`, searchBirdNames)
.get(`${BASE_ROUTE}/users`, searchUsers)
.get(`${BASE_ROUTE}/pictures`, searchPictures)

module.exports = router;
