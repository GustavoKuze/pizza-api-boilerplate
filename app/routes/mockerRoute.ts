import { Router } from "express";
import {
  generateFakeUsers,
  generateFakePictures,
} from "@controllers/mockerController";

const router = Router();

const BASE_ROUTE = "/mock";

router
  .get(`${BASE_ROUTE}/generate/users`, generateFakeUsers)
  .get(`${BASE_ROUTE}/generate/pictures`, generateFakePictures);

module.exports = router;
