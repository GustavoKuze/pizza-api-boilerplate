/* eslint-disable import/no-dynamic-require, global-require */
import express from "express";
import path from "path";
import fs from "fs";

const router = express.Router();
fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== "index.ts" && file !== "index.js"
  )
  .forEach((file) => router.use(require(path.resolve(__dirname, file))));

module.exports = router;
