import { Router } from "express";

const router = Router();

import {
  createNewPicture,
  addComment,
  listComments,
  like,
  removeLike,
  getUserPictures,
  getLikesDetails,
  getBirdDetails,
} from "@controllers/feedController";

import userExists from "@middlewares/userExists";

const BASE_ROUTE = "/feed";

router
  .post(`${BASE_ROUTE}/picture`, createNewPicture)
  .post(`${BASE_ROUTE}/comment`, addComment)
  .get(`${BASE_ROUTE}/comment/:pictureId`, listComments)
  .get(`${BASE_ROUTE}/bird/:birdId`, getBirdDetails)
  .get(`${BASE_ROUTE}/user/pictures/:userId`, userExists, getUserPictures)
  .get(`${BASE_ROUTE}/like/details`, getLikesDetails)
  .put(`${BASE_ROUTE}/like`, userExists, like)
  .put(`${BASE_ROUTE}/remove-like`, userExists, removeLike);

module.exports = router;
