import { Router } from "express";

const router = Router();

import {
  signUp,
  getAll,
  login,
} from "@controllers/userController";
import authenticate from "@middlewares/auth";

const BASE_ROUTE = "/user";

router
  .post(BASE_ROUTE, signUp)
  .post(`${BASE_ROUTE}/login`, login)
  .get(`${BASE_ROUTE}/`, getAll);

module.exports = router;
