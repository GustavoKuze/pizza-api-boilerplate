import { Router } from 'express';
import { 
  createOrder,
  getOrder,
  listOrders,
} from '@controllers/orderController';

const router = Router();

const BASE_ROUTE = '/order';

router
  .post(`${BASE_ROUTE}/order`, createOrder)
  .get(`${BASE_ROUTE}/order/:id`, getOrder)
  .get(`${BASE_ROUTE}/order`, listOrders);

module.exports = router;
