import bc from "bcryptjs";

const hashPassword = async (password: string) => {
  const salt = await bc.genSalt(10);
  return bc.hash(password, salt);
};

const validatePassword = (password: string, hashed: string) =>
  bc.compare(password, hashed);

export { hashPassword, validatePassword };
