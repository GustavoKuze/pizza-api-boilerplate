import sharp from "sharp";
import fs from "fs";
import { formatDistanceToNow, parseISO } from "date-fns";
import { ptBR } from "date-fns/locale";

const compressImage = async (file: any, size?: number) => {
  const newPath = file.path;

  const data = await sharp(file.path)
    .resize(size)
    .toFormat("jpg")
    .jpeg()
    .toBuffer();

  fs.writeFile(newPath, data, (err) => {
    if (err) throw err;
  });

  return newPath;
};

const getFriendlyDate = (isoDate) => formatDistanceToNow(typeof isoDate === 'string' ? parseISO(isoDate.replace('+00:00', '')) : isoDate, { locale: ptBR })

export { compressImage, getFriendlyDate };
