import { Response } from "express";

const okResponse = (res: Response, data: any) =>
  res.status(200).json({
    data,
    error: "",
  });

const badResponse = (res: Response, error: string) =>
  res.status(400).json({
    data: null,
    error,
  });

const unauthorizedResponse = (res, error: string) =>
  res.status(401).json({
    data: null,
    error,
  });

export { okResponse, badResponse, unauthorizedResponse };
