/* eslint-disable consistent-return */
import { badResponse } from "@utils/response";
import UserRepository from "@repository/UserRepository";

const userExists = async (req, res, next) => {
  try {
    const userId = req.params.userId || req.query.userId || req.body.userId;

    if (!userId) throw new Error("Usuário não encontrado");

    const userRepo = new UserRepository();

    const user = await userRepo.getById(userId);

    if (!user) throw new Error("Usuário não encontrado");

    next();
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export default userExists;
