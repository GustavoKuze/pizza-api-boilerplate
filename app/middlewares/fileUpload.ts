const multer = require("multer");
const path = require("path");

const fileUploadMiddleware = multer({
  storage: multer.diskStorage({
    destination: (req: any, file: any, cb: any) => {
      cb(null, path.resolve(__dirname, "../../media"));
    },
    filename: (req: any, file: any, cb: any) => {
      const ext =
        file.originalname.split(".").length > 1
          ? `.${file.originalname.split(".")[1]}`
          : "";
      cb(null, `${Date.now().toString()}${ext}`);
    },
  }),
  fileFilter: (req: any, file: any, cb: any) => {
    const acceptedFileFormats = ["image/png", "image/jpg", "image/jpeg"];
    const isAccepted = acceptedFileFormats.includes(file.mimetype);

    return cb(null, isAccepted);
  },
});

export default fileUploadMiddleware;
