/* eslint-disable consistent-return */
import jwt from "jsonwebtoken";
import { unauthorizedResponse } from "@utils/response";

const authenticate = (req, res, next) => {
  try {
    let token = req.headers["x-access-token"] || req.headers.authorization;
    if (token && token.startsWith("Bearer "))
      token = token.slice(7, token.length);

    if (!token) {
      return unauthorizedResponse(res, "Acesso Negado");
    }
    const authenticated = jwt.verify(
      token,
      process.env.SECRET,
      (err, decoded) => {
        if (err) throw new Error("Token expirado");
      }
    );
    req.user = authenticated;
    next();
  } catch (err) {
    return unauthorizedResponse(res, err.message);
  }
};

export default authenticate;
