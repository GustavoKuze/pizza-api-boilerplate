import { Request, Response } from "express";
import { badResponse, okResponse } from "@utils/response";
import MockerRepository from "@repository/MockerRepository";

const generateFakeUsers = async (req: Request, res: Response) => {
  try {
    const repo = new MockerRepository();

    const results = await repo.generateFakeUsers(
      parseInt(`${req.query.quantity}`, 10) || 10
    );

    return okResponse(res, results);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const generateFakePictures = async (req: Request, res: Response) => {
  try {
    const repo = new MockerRepository();

    const results = await repo.generateFakePictures(
      parseInt(`${req.query.quantity}`, 10) || 10,
      Boolean(req.query.withComments)
    );

    return okResponse(res, results);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export { generateFakeUsers, generateFakePictures };
