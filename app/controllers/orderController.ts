import { Request, Response } from "express";
import { badResponse, okResponse } from "@utils/response";
import OrderRepository from "@repository/OrderRepository";

const createOrder = async (req: Request, res: Response) => {
  try {
    const repo = new OrderRepository();

    const userCreated = await repo.createOrder(req.body);

    return okResponse(res, userCreated);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const listOrders = async (req: Request, res: Response) => {
  try {
    const repo = new OrderRepository();
    const list = await repo.listOrders(
      parseInt(`${req.params.userId}`, 10),
      parseInt(`${req.query.page || '1'}`, 10),
    );

    return okResponse(res, list);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const getOrder = async (req: Request, res: Response) => {
  try {
    const repo = new OrderRepository();

    const details = await repo.getOrder(parseInt(`${req.params.id}`, 10));

    return okResponse(res, details);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export { createOrder, listOrders, getOrder };
