import { Request, Response } from "express";
import UserRepository from "@repository/UserRepository";
import { badResponse, okResponse } from "@utils/response";
import { hashPassword, validatePassword } from "@utils/hasher";
import jwt from "jsonwebtoken";
import chance from "@services/chance";

const signUp = async (req: Request, res: Response) => {
  try {
    const repo = new UserRepository();

    const doesEmailExist = await repo.isEmailRegistered(req.body.email);

    if (doesEmailExist) {
      return badResponse(res, "Esse E-mail já esta cadastrado");
    }

    const hashed = await hashPassword(req.body.password);

    const userRegistered = await repo.signUp({
      ...req.body,
      password: hashed,
      username: `${req.body.name.replace(/[ ]/gm, '')}#${chance.integer({ min: 1, max: 99999 })}`,
    });

    return okResponse(res, userRegistered);
  } catch (err) {
    return res.status(400).json({ error: err.message });
  }
};

const login = async (req, res) => {
  try {
    const repo = new UserRepository();

    const user = await repo.getByEmail(req.body.email);

    if (!user) {
      return badResponse(
        res,
        "E-mail não encontrado. Caso seu usuário tenha sido desabilitado, entre em contato com a administração para habilitá-lo novamente."
      );
    }

    const isPasswordValid = await validatePassword(
      req.body.password,
      user.password || ""
    );

    if (!isPasswordValid) {
      return badResponse(res, "E-mail ou senha incorretos");
    }

    // eslint-disable-next-line no-underscore-dangle
    const token = jwt.sign(
      {
        userId: user.id,
        currentAuthority: "user",
      },
      process.env.SECRET,
      {
        expiresIn: "7d",
        subject: `${user.id}`,
      }
    );
    return res
      .header("access_token", token)
      .header("access_level", "user")
      .json({
        token,
        user: {
          currentAuthority: "user",
          email: user.email,
          userId: user.id,
        },
      });
  } catch (err) {
    console.log(err);

    return badResponse(
      res,
      err.message || "Ocorreu um erro ao tentar fazer login"
    );
  }
};

const getAll = async (req: Request, res: Response) => {
  try {
    const repo = new UserRepository();
    const user = await repo.getAll();
    return okResponse(res, user);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export { signUp, getAll, login };
