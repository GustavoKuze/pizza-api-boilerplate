import { Request, Response } from "express";
import { badResponse, okResponse } from "@utils/response";
import SearchRepository from "@repository/SearchRepository";

const searchBirdNames = async (req: Request, res: Response) => {
  try {
    const repo = new SearchRepository();
    const birdNames = await repo.searchBirds(`${req.query.name}`);

    return okResponse(res, birdNames);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const searchUsers = async (req: Request, res: Response) => {
  try {
    const repo = new SearchRepository();
    const birdNames = await repo.searchUsers(`${req.query.name}`);

    return okResponse(res, birdNames);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const searchPictures = async (req: Request, res: Response) => {
  try {
    const repo = new SearchRepository();
    const birdNames = await repo.searchPictures(`${req.query.name}`);

    return okResponse(res, birdNames);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export { searchBirdNames, searchUsers, searchPictures };
