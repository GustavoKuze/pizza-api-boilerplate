import { Request, Response } from "express";
import FeedRepository from "@repository/FeedRepository";
import { badResponse, okResponse } from "@utils/response";
import Like from "@models/Like";

const createNewPicture = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const userRegistered = await repo.createNewPicture(req.body);

    return okResponse(res, userRegistered);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const addComment = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const addedComment = await repo.createNewComment(req.body);

    return okResponse(res, addedComment);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const listComments = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const comments = await repo.getAllComments(
      parseInt(req.params.pictureId, 10),
      parseInt(`${req.query.limit || 0}`, 10),
      Boolean(req.query.showAll)
    );

    return okResponse(res, comments);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const like = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const isLikeOk = await repo.like(req.body);

    return okResponse(res, isLikeOk);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const removeLike = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const isRemoveLikeOk = await repo.removeLike(req.body);

    return okResponse(res, isRemoveLikeOk);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const getUserPictures = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const isLikeOk = await repo.getUserPictures(
      parseInt(`${req.params.userId}`, 10),
      parseInt(`${req.query.page || '1'}`, 10),
    );

    return okResponse(res, isLikeOk);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const getLikesDetails = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const details = await repo.getLikesDetails(
      new Like(
        0,
        0,
        req.query.pictureId ? parseInt(`${req.query.pictureId}`, 10) : 0,
        req.query.commentId ? parseInt(`${req.query.commentId}`, 10) : 0,
      )
    );

    return okResponse(res, details);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

const getBirdDetails = async (req: Request, res: Response) => {
  try {
    const repo = new FeedRepository();

    const details = await repo.getBirdDetails(
      parseInt(req.params.birdId, 10),
    );

    return okResponse(res, details);
  } catch (err) {
    return badResponse(res, err.message);
  }
};

export {
  createNewPicture,
  addComment,
  listComments,
  like,
  getUserPictures,
  removeLike,
  getLikesDetails,
  getBirdDetails,
};
