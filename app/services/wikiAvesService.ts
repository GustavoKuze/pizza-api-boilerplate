import axios from "axios";

import { WIKI_AVES_BASE_URL } from "@constants/wikiAves";

const searchBirdNames = async (q) => {
  const { data, statusText } = await axios.get(
    `${WIKI_AVES_BASE_URL}/getTaxonsJSON.php?term=${q}`
  );

  if (!(statusText === "OK")) return [];

  return data.map((bird) => ({
    id: bird.id,
    wikiId: bird.wid,
    label: bird.label,
    name: bird.nome,
  }));
};

const getBirdIdByCommonName = async (commonName) => {
  const birdNames = await searchBirdNames(commonName);

  const birdInfo = birdNames.find((bird) => bird.wikiId === commonName);

  return birdInfo.id;
};

export { searchBirdNames, getBirdIdByCommonName };
