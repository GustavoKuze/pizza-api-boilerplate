const WIKI_AVES_BASE_URL = "https://www.wikiaves.com.br";
const BIRD_REGISTERS_PAGE_SIZE = 20;

export { WIKI_AVES_BASE_URL, BIRD_REGISTERS_PAGE_SIZE };
