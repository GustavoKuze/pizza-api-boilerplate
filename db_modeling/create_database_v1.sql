-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema pizza_boilerplate
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pizza_boilerplate
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pizza_boilerplate` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema pizza_boilerplate
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pizza_boilerplate
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pizza_boilerplate` DEFAULT CHARACTER SET latin1 ;
USE `pizza_boilerplate` ;

-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`payment_method`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`payment_method` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`card_brand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`card_brand` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `logo` VARCHAR(256) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(65) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `phone` VARCHAR(15) NULL DEFAULT NULL,
  `disabled` TINYINT(1) NULL DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `password` VARCHAR(150) NOT NULL,
  `avatar` VARCHAR(256) NULL DEFAULT NULL,
  `username` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`credit_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`credit_card` (
  `id` INT(11) NOT NULL,
  `number` VARCHAR(16) NOT NULL,
  `name` VARCHAR(65) NOT NULL,
  `cpf_cnpj` VARCHAR(14) NOT NULL,
  `expiration` VARCHAR(5) NOT NULL,
  `cvv` VARCHAR(5) NOT NULL,
  `card_brand_id` INT NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `card_brand_id`, `user_id`),
  INDEX `fk_credit_card_card_brand_idx` (`card_brand_id` ASC),
  INDEX `fk_credit_card_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_credit_card_card_brand`
    FOREIGN KEY (`card_brand_id`)
    REFERENCES `pizza_boilerplate`.`card_brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_credit_card_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `pizza_boilerplate`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `description` VARCHAR(300) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `image` VARCHAR(256) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`product_variation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`product_variation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `label` VARCHAR(45) NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `product_id` INT NOT NULL,
  PRIMARY KEY (`id`, `product_id`),
  INDEX `fk_product_variation_product1_idx` (`product_id` ASC),
  CONSTRAINT `fk_product_variation_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `pizza_boilerplate`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `street` VARCHAR(100) NOT NULL,
  `neighborhood` VARCHAR(75) NOT NULL,
  `complement` VARCHAR(30) NULL DEFAULT NULL,
  `city` VARCHAR(65) NOT NULL,
  `state` VARCHAR(2) NOT NULL,
  `country` VARCHAR(2) NULL DEFAULT 'BR',
  `number` VARCHAR(11) NOT NULL,
  `cep` VARCHAR(9) NOT NULL,
  `user_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `user_address_fk_idx` (`user_id` ASC),
  CONSTRAINT `user_address_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `pizza_boilerplate`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  `payment_method_id` INT NOT NULL,
  `card_id` INT NULL,
  `address_id` INT NOT NULL,
  `status` VARCHAR(25) NULL DEFAULT 'AGUARDANDO_CONFIRMACAO',
  PRIMARY KEY (`id`, `user_id`, `payment_method_id`),
  INDEX `fk_order_user1_idx` (`user_id` ASC),
  INDEX `fk_order_payment_method1_idx` (`payment_method_id` ASC),
  INDEX `fk_selected_card_idx` (`card_id` ASC),
  INDEX `fk_selected_address_idx` (`address_id` ASC),
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `pizza_boilerplate`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_payment_method1`
    FOREIGN KEY (`payment_method_id`)
    REFERENCES `pizza_boilerplate`.`payment_method` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_selected_card`
    FOREIGN KEY (`card_id`)
    REFERENCES `pizza_boilerplate`.`credit_card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_selected_address`
    FOREIGN KEY (`address_id`)
    REFERENCES `pizza_boilerplate`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`cart_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`cart_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `quantity` INT NOT NULL,
  `product_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`id`, `product_id`),
  INDEX `fk_cart_item_product1_idx` (`product_id` ASC),
  INDEX `fk_order_idx` (`order_id` ASC),
  CONSTRAINT `fk_cart_item_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `pizza_boilerplate`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order`
    FOREIGN KEY (`order_id`)
    REFERENCES `pizza_boilerplate`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`discount_coupon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`discount_coupon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(25) NOT NULL,
  `discount_percentage` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizza_boilerplate`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pizza_boilerplate`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_id` INT NOT NULL,
  `coupon_id` INT NULL,
  `status` VARCHAR(25) NULL DEFAULT 'PENDENTE',
  PRIMARY KEY (`id`),
  INDEX `fk_order_idx` (`order_id` ASC),
  INDEX `fk_discount_idx` (`coupon_id` ASC),
  CONSTRAINT `fk_order_payment`
    FOREIGN KEY (`order_id`)
    REFERENCES `pizza_boilerplate`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_discount_payment`
    FOREIGN KEY (`coupon_id`)
    REFERENCES `pizza_boilerplate`.`discount_coupon` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `pizza_boilerplate` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
